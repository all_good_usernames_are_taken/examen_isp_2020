public class AThread1 extends Thread {
    String executionThreadName = "AThread1";

    /**
     * Prints a message each three seconds to the console
     * The message format is [execution_thread_name] - [message_number], where [execution_thread_name] is the name of
     * the thread, and [message_number] is the index of the message
     */
    @Override
    public void run() {
        for (int i = 0; i < 8; ++i) {
            System.out.println(executionThreadName + " - " + i);
            try {
                // Sleep 3 seconds after each message
                Thread.sleep(3000);
            } catch (Exception e) {
                System.out.println("Exception occurred");
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        AThread1 thread1 = new AThread1();
        AThread2 thread2 = new AThread2();

        thread1.start();
        thread2.start();
    }
}

class AThread2 extends Thread {
    String executionThreadName = "AThread2";

    /**
     * Prints a message each three seconds.
     * The message format is [execution_thread_name] - [message_number], where [execution_thread_name] is the name of
     * the thread, and [message_number] is the index of the message
     */
    @Override
    public void run() {
        for (int i = 0; i < 8; ++i) {
            System.out.println(executionThreadName + " - " + i);
            try {
                // Sleep 3 seconds after each message
                Thread.sleep(3000);
            } catch (Exception e) {
                System.out.println("Exception occurred");
                e.printStackTrace();
            }
        }
    }
}