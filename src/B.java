// A depends on B
// B aggregates D
// B is associated with E
public class B {
    // B has an attribute of type D, but does not have an exclusive hold on it.
    // d's lifecycle does not depend on B's lifecycle.
    private long t;
    D d;

    // Sets the d attribute. Because D is not created by B itself, it must be set from an external scope.
    public void setD(D d) {
        this.d = d;
    }

    public void x() {

    }
}

class A {
    // Class A depends on B, so it has a method that uses an object of class B as a parameter.
    public void met(B b) {
    }
}


class D {
}

interface C {
}

class F {
    public void metA() {
    }
}

// Composed of F (one F object)
// Implements C
class E implements C {
    F f;

    // Constructor creates an object of class F, of which the class E is composed.
    // The object f will be destroyed once E is destroyed
    public E() {
        f = new F();
    }

    public void metG(int i) {
    }
}
